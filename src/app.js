import page from 'page';
import checkConnectivity from './network.js';
import { fetchTodos, fetchTodo,deleteTodo } from './api/todo.js';
import { setTodos,setTodo, getTodos, unsetTodo} from './idb.js';
import Edit from './views/edit.js';

checkConnectivity({});
document.addEventListener('connection-changed', e => {
  document.offline = !e.detail;
  if (!document.offline) {
    // Sync data ...
  }
});



const app = document.querySelector('#app');
fetch('./config.json')
  .then(result => result.json())
  .then(async (config) => {
    // Set the config
    console.log("[todo] Config loaded !!!");
    window.config = config;
  

    page('/', async (ctx) => {
      const link = document.createElement('link');
      link.setAttribute('rel', 'stylesheet');
      link.setAttribute('href', './assets/styles/tailwind.css');
      document.head.appendChild(link);
        const module = await import('./views/home.js');
      const Home = module.default;

      let todos = [];
      if (navigator.onLine) {
        const data = await fetchTodos();
        if (data !== false) {
          todos = await setTodos(data);
        }
        todos = await getTodos();
      } else {
        todos = await getTodos();
      }

       const ctn = app.querySelector('[page="home"]');
       const homeView = new Home(ctn);
       homeView.todos = todos;
       homeView.renderView();
       ctn.setAttribute('active', true); 

      document.addEventListener("delete-todo", async ({ detail }) => {
        if (!document.offline && navigator.onLine === true) {
          const result = await deleteTodo(detail.id);
          if (result !== false) {
            const todo = await unsetTodo(detail.id);
            return document.dispatchEvent(new CustomEvent("render-view", { detail: todo }));
          }
          detail.deleted = "true";
        }
      });


    });

   page();

  });


function displayPage (page) {
  const skeleton = document.querySelector("#app .skeleton");
  skeleton.removeAttribute("hidden");
  const pages = app.querySelectorAll("[page]");
  pages.forEach(page => page.removeAttribute("active"));
  skeleton.setAttribute("hidden", "true");
  const p = app.querySelector(`[page="${page}"]`);
  p.setAttribute("active", true);
}
