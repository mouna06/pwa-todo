import { render, html } from 'lit-html';
import '../component/todo-card.js';
import { setTodo, getTodos, unsetTodo} from '../idb.js';

import 'lit-icon';



export default class Home {
  constructor(page) {
    this.page = page;
    this.properties = {
      todo: '',
      description: '',
      todos: []
    };

    this.renderView();
  }

  set todos(value) {
    this.properties.todos = value;
  }

  get todos() {
    return this.properties.todos;
  }

  template() {
    return html`
      <section class="h-full">
        <div ?hidden="${!this.todos.length}">
          <header>
            <h1 class="mt-2 px-4 text-xl">My awesome todos : </h1>
          </header>
          <main class="todolist px-4 pb-20">
            <ul id ="forlist">
              ${this.todos.map(todo => html`<todo-card .todo="${todo}"></todo-card>`)}
          
              </ul>
          </main>
        </div>
        <div class="mt-8" ?hidden="${!!this.todos.length}">
          <p class="mt-4 text-center text-xl">No todos yet, try to create a new one</p>
        </div>
        <footer class="h-16 bg-gray-300 fixed bottom-0 inset-x-0">
        <form @submit="${this.handleForm.bind(this)}" id="addTodo" class="w-full h-full flex justify-between items-center px-4 py-3">
        <input
      id="add-item-field-1" 
      class="py-3 px-4 add-item-field"
      .value="${this.properties.todo}"
      @input="${e => this.properties.todo = e.target.value}"
        type="text"
        placeholder="Enter a title ..."
        name="todo">
       <input
    id="add-item-field-2" 
      class="py-3 px-4 add-item-field"
      .value="${this.properties.description}"
      @input="${e => this.properties.description = e.target.value}"
      type="text"
      placeholder="Enter a description ..."
      name="todo">
  <button type="submit" class="cta-submit">
  Ajouter
</button>
  </form>  
</footer>
      </section>
      <lit-iconset>
        <svg><defs>
          <g id="delete"><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"></path></g>
          <g id="cloud-off"><path d="M19.35 10.04C18.67 6.59 15.64 4 12 4c-1.48 0-2.85.43-4.01 1.17l1.46 1.46C10.21 6.23 11.08 6 12 6c3.04 0 5.5 2.46 5.5 5.5v.5H19c1.66 0 3 1.34 3 3 0 1.13-.64 2.11-1.56 2.62l1.45 1.45C23.16 18.16 24 16.68 24 15c0-2.64-2.05-4.78-4.65-4.96zM3 5.27l2.75 2.74C2.56 8.15 0 10.77 0 14c0 3.31 2.69 6 6 6h11.73l2 2L21 20.73 4.27 4 3 5.27zM7.73 10l8 8H6c-2.21 0-4-1.79-4-4s1.79-4 4-4h1.73z"></path></g>
          <g id="send"><path d="M2.01 21L23 12 2.01 3 2 10l15 2-15 2z"></path></g>
          <g id="check"><path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z"/></g>
          <g id="close"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/></g>
          <g id="arrow_back"><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></g>
        </defs></svg>
      </lit-iconset>
    `;
  }

  renderView() {
    const view = this.template();
    render(view, this.page);
  }

  handleForm(e) {
    e.preventDefault();
    let dateNow = new Date().getTime();
    let item = {
        id: dateNow,
        title: this.properties.todo,
        description: this.properties.description,
        async : false,
        isDeleted: false,
        isUpdate: false,
        date: dateNow
    }
    let newItem = '<todo-card .todo="${todo}"></todo-card>';
     document.getElementById("forlist").insertAdjacentHTML('beforeend', newItem);
    setTodo(item);
    createTodo(item.id);
    this.renderView();
  }
}
