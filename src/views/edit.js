import { render, html } from "lit-html";
import "../component/todo-card.js";
import "lit-icon";

export default class Edit {
  constructor (page) {
    this.page = page;
    this.properties = {
      todo: "",
    };

    this.renderView();
  }

  set todo (value) {
    this.properties.todo = value;
  }

  get todo () {
    return this.properties.todo;
  }

  template () {
    return html`
      <section class="h-full container px-5 mt-5 mx-auto">
        <a class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 mb-3 rounded inline-flex items-center" href="/">
          <lit-icon icon="arrow_back"></lit-icon>         
          <span class="ml-2">Back</span>
        </a>
          <div class="max-w text-center bg-gray-300 rounded overflow-hidden shadow-lg">
              <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2">${this.todo.title}</div>
                <p class="text-gray-700 text-base">
                  ${this.todo.description}
                </p>
                <span class="align-text-bottom">Status : </span><lit-icon icon="${this.todo.done === "true" ? "check" : "close"}"></lit-icon>
              </div>
            </div>
        </main>
      </section>
      `;
  }

  renderView () {
    const view = this.template();
    render(view, this.page);
  }
}
